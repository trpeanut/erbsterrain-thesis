#ifdef PBLENDINGCURVE_H
//#include "pblendingcurve.h"


template<typename T>
PBlendingCurve<T>::PBlendingCurve( GMlib::DVector< GMlib::Vector<T, 3> > c, int t)
{
    isBezier = false;
    _c = c;
    this->generateKnotVector();
    //this->generateSubCurve(c,n);
    //this->displayLocalCurves();
}

template<typename T>
PBlendingCurve<T>::PBlendingCurve( GMlib::DVector< GMlib::Vector<T, 3> > c, int d, int t)
{
    _d = d;
    _c.setDim(c.getDim());
    _c = c;
    _closed = false;
    _bst = false;
    isBezier = false;

    this->generateBSplineCurve();
    this->generateKnotVector();
    _ct = PointCurveType(t);
}

template<typename T>
PBlendingCurve<T>::PBlendingCurve( GMlib::DVector< GMlib::Vector<T, 3> > c, int d, int t, bool f)
{
    _d = d;
    _c.setDim(c.getDim());
    _c = c;
    _closed = false;
    _bst = f;

    this->generateBSplineCurve();
    this->generateKnotVector();
    _ct = PointCurveType(2);
}

template<typename T>
PBlendingCurve<T>::PBlendingCurve(const PBlendingCurve<T> & copy)
{
    //this = copy;
}

template<typename T>
PBlendingCurve<T>::~PBlendingCurve()
{

}


template<typename T>
void PBlendingCurve<T>::generateKnotVector()
{
    int n = _lc.getDim();
    T start = 0;
    T end = 1;

    T delta = (end-start)/(n-1);

    _kv.setDim(n+2);

    for(int i=0; i<n; i++)
    {
        _kv[i+1] = start+ i*delta;

    }

    if(_closed)
    {
        _kv[0] = _kv[1] - (_kv[n] - _kv[n-1]);
        _kv[n+1] = _kv[n] + (_kv[2] - _kv[1]);
    }
    else
    {
        _kv[0] = _kv[1];
        _kv[n+1] = _kv[n];
    }

}

template<typename T>
void PBlendingCurve<T>::errorDiff()
{
    GMlib::DVector< GMlib::Vector<T,3>> error;
    error.setDim(_c.getDim());
    T t;
    T d = T(1) / T(_c.getDim());
    for (int i = 0; i < _c.getDim(); i++)
    {
            t = T(i) * T(d);
            GMlib::Point<T, 3> closest;
            const GMlib::Point<T, 3> find = GMlib::Point<T, 3>(_c[i]);
            this->getClosestPoint(find, t, closest, 10e-6, 10);
            error[i] = GMlib::Vector<T, 3>(closest);
    }
    _e.setDim(error.getDim());
    _e = error;
}

template<typename T>
GMlib::Vector<T, 3> PBlendingCurve<T>::getSumError()
{
    int skips=0;
    GMlib::Vector<T,3> se = {T(0), T(0), T(0)};
    for (int i = 0; i < _c.getDim(); i++)
    {
            const GMlib::Point<T, 3> find = GMlib::Point<T, 3>(_c[i]);

            if (!std::isnan(_e[i][0]))
            {
                auto diff = find - _e[i];
                if (std::abs(diff[0]) > 100 || std::abs(diff[1]) > 100 || std::abs(diff[2]) > 100)
                {
                    skips++;
                }
                else
                {
                    se+=(find - _e[i]);
                }
            }
            else
            {
                skips++;
            }
    }
    _es = skips;
    return se;
}

template<typename T>
int PBlendingCurve<T>::getErrorSkips()
{
    return _es;
}

template<typename T>
int PBlendingCurve<T>::getPointCount()
{
    return _c.getDim();
}

template<typename T>
void PBlendingCurve<T>::generateBSplineCurve()
{

    int i = 0;
    int s = std::floor(_c.getDim() /2) - 1;
    int x = _c.getDim() % 2;

    _lc.setDim(s);
    for(; i<s-1; i++)
    {
        GMlib::DVector< GMlib::Vector<T, 3> > c;
        c.setDim(4);
        for (int j = 0; j< (4); j++)
        {
             c[j] = _c[(i*2)+j];
        }
        _lc[i] = new GMlib::PBSplineCurve<T>(c, 3, _bst);
    }

    GMlib::DVector< GMlib::Vector<T, 3> > c;
    c.setDim(4+x);
    for (int j = 0; j < (4 + x); j++)
    {
        c[j] = _c[(i*2)+j];
    }
    _lc[i] = new GMlib::PBSplineCurve<T>(c, 3, _bst);

}


template<typename T>
T PBlendingCurve<T>::getScalar(T t, int index) const
{
    if(isBezier)
    {
        return (t-_kv[index-1])/(_kv[index + 1] - _kv[index-1]);
    }
    else
    {
        return t;
    }
}

template<typename T>
void PBlendingCurve<T>::eval( T t, int d, bool /*l*/ ) const
{
    //calculating index
    this->_p.setDim( d + 1 );

    //find index
    int index=1 ;

    for(index=1; index< _kv.getDim()-2; index++)
    {
        if(t >= _kv[index] && t < _kv[index+1])
        {
            break;
        }
    }
    if(index>_kv.getDim()-3)
    {
        index=_kv.getDim()-3;
    }

    T scale = _kv[index+1] - _kv[index];

    GMlib::DVector<T> Bfunc = B((t-_kv[index])/(_kv[index+1]-_kv[index]), scale);
    GMlib::PCurve<T,3> *c1 = _lc[index-1];
    GMlib::PCurve<T,3> *c2 = _lc[index];


    GMlib::DVector<GMlib::Vector<T,3> > ec1= c1-> evaluateParent(getScalar(t,index),d);
    GMlib::DVector<GMlib::Vector<T,3> > ec2= c2-> evaluateParent(getScalar(t,index+1),d);


    this->_p[0] = ec1[0]+ Bfunc[0]*(ec2[0] - ec1[0]);

    if( d > 0)
    {
        this->_p[1] = ec1[1]+ Bfunc[0]*( ec2[1] - ec1[1]) + Bfunc[1]*( ec2[0] -  ec1[0]);
    }

    if( d > 1)
    {
        this->_p[2] = ec1[2]+ Bfunc[0]*( ec2[2] -  ec1[2]) + 2*Bfunc[1]*( ec2[1] -  ec1[1]) + Bfunc[2]*(ec2[0] -  ec1[0]);
    }
}


template<typename T>
GMlib::DVector<T> PBlendingCurve<T>::B(T t, T s) const
{
    //B function of order 1 3tsq-2tcube
    GMlib::DVector<T> b(_d+1);

    b[0]= 3*pow(t,2)-2*pow(t,3);

    if(_d>0)
    {
        b[1]= 6*t-6*pow(t,2) / s; // first derivative
    }
    if(_d>1)
    {
        b[2]= (6-12*t) / pow(s, 2); // 2nd  derivative
    }
    if(_d>2)
    {
        b[3]= -12 / pow(s, 3); // 3rd derivative
    }

    return(b);
}

template<typename T>
T PBlendingCurve<T>::getEndP() const
{
    return _kv[_kv.getDim()-2];
}

template<typename T>
T PBlendingCurve<T>::getStartP() const
{
    return _kv[1];
}

template<typename T>
bool PBlendingCurve<T>::isClosed() const
{
    return _closed;
}

template<typename T>
void PBlendingCurve<T>::localSimulate(double dt)
{

}

template<typename T>
void PBlendingCurve<T>::displayLocalCurves()
{
    for (int i = 0; i < _lc.getDim() - 1; i++)
    {
        this->insert(_lc[i]);
        _lc[i]->toggleDefaultVisualizer();
        //_lc[i]->replot(50,1);
        _lc[i]->setColor(GMlib::GMcolor::red());
        //_lc[i]->translate(T(0), T(3));

    }
}


#endif
