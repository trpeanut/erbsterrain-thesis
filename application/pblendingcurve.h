#ifndef PBLENDINGCURVE_H
#define PBLENDINGCURVE_H

#include <parametrics/gmpbsplinecurve>
//#include "bsplinecurve.h"

enum class PointCurveType { Sub = 0, Bezier = 1, BSpline = 2, CTBSpline = 3 };

template<typename T>
class PBlendingCurve: public GMlib::PCurve<T,3>{

    GM_SCENEOBJECT(PBlendingCurve)

public:
    PBlendingCurve( GMlib::DVector< GMlib::Vector<T, 3> > c, int t );
    PBlendingCurve( GMlib::DVector< GMlib::Vector<T, 3> > c, int d, int t );
    PBlendingCurve( GMlib::DVector< GMlib::Vector<T, 3> > c, int d, int t, bool f); //BSpline Constructor
    PBlendingCurve( const PBlendingCurve<T>& copy );

    virtual ~PBlendingCurve();

    bool   isClosed() const;
    void  displayLocalCurves();
    void errorDiff();
    GMlib::Vector<T, 3>   getSumError();
    int  getErrorSkips();
    int  getPointCount();

protected:

    void    eval(T t, int d, bool l = true ) const;
    T       getEndP() const;
    T       getStartP() const;
    //void    resample( GMlib::DVector< GMlib::DVector< GMlib::Vector<T,3> > >& p, int m, int d, T start, T end );

private:
    void  generateKnotVector();
    void  generateBSplineCurve();

    T  getScalar(T t, int index) const; //Returns scalars for blending function

    void  localSimulate(double dt);

    GMlib::DVector<T> B(T t, T s) const; //blending function

    GMlib::DVector<T> _kv; //knot vector
    GMlib::DVector<GMlib::PCurve<T,3>*> _lc; //local curves
    GMlib::DVector< GMlib::Vector<T, 3>> _c; //Point vector
    GMlib::DVector< GMlib::Vector<T,3>> _e; //Error Vector
    int _es; //Error sum skips
    bool _closed; //True for closed, false for open
    bool isBezier; //True for bezier, false for subcurve
    double _sum; //Sum value for local simulate
    PointCurveType _ct;
    int _d;
    bool _bst = false;



};

#include "pblendingCurve.c"

#endif // PBLENDINGCURVE_H
