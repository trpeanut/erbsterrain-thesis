#ifdef POINTGRID_H
//#include "pointgrid.h"


template<typename T>
PointGrid<T>::PointGrid()
{

}

template<typename T>
void PointGrid<T>::loadFile(std::string f)
{
    std::ifstream inFile(f);
       if (inFile.is_open())
       {
           int oldFID = -100;
           int i = 0;
           float lowX = 100000000;
           float lowY = 100000000;
           float highZ = 0;
           std::string line;
           GMlib::DVector<GMlib::Vector<T,3>> cl;
           cl.resetDim(0);

           std::string OBJECTID, HOYDE, x, y, ORIG_FID;
           int ORIG_FIDi;
           float HOYDEf, xf, yf ;

           std::getline(inFile,line);

           while( std::getline(inFile,line) )
           {
               std::istringstream ss(line);

               std::getline(ss,OBJECTID,',');
               std::getline(ss,HOYDE,',');
               std::getline(ss,x,',');
               std::getline(ss,y,',');
               std::getline(ss,ORIG_FID,',');

               ORIG_FIDi = stoi(ORIG_FID);
               HOYDEf = stof(HOYDE);
               xf = stof(x);
               yf = stof(y);

               if (ORIG_FIDi != oldFID)
               {
                   if (oldFID >= 0)
                   {
                        _cl.push_back(cl);
                   }
                   cl.resetDim(0);
                   oldFID = ORIG_FIDi;
                   i = 0;
               }

               if (xf < lowX)
               {
                   lowX = xf;
               }
               if (yf < lowY)
               {
                   lowY = yf;
               }
               if (HOYDEf > highZ)
               {
                   highZ = HOYDEf;
               }

               auto vec = GMlib::Vector<T,3>(xf,yf,HOYDEf);
               cl.append(vec);
               i++;
           }
           _cl.push_back(cl);
           _lX = lowX;
           _lY = lowY;
           _hZ = highZ;
           std::cout << highZ << std::endl;
       }
       inFile.close();
}

template<typename T>
void PointGrid<T>::setData(std::vector<GMlib::DVector<GMlib::Vector<T,3>>> cl)
{
    this->_cl = cl;
}

template<typename T>
void PointGrid<T>::normalize()
{
    for (uint i = 0; i<_cl.size(); i++)
    {
        for (int j = 0; j < _cl[i].getDim(); j++)
        {
            _cl[i][j][0] = (_cl[i][j][0] - _lX) /100;
            _cl[i][j][1] = (_cl[i][j][1] - _lY) /100;
            _cl[i][j][2] = (_cl[i][j][2] - _hZ) /100;
        }
    }
}

template<typename T>
std::vector<GMlib::DVector<GMlib::Vector<T,3>>> PointGrid<T>::getData()
{
    return _cl;
}



#endif
