#ifndef POINTGRID_H
#define POINTGRID_H

#include <gmCoreModule>
#include <fstream>
#include <iostream>
#include <string>

template<typename T>
class PointGrid
{
public:
    PointGrid();
    ~PointGrid();

    void loadFile(std::string f);
    void setData(std::vector<GMlib::DVector<GMlib::Vector<T,3>>> cl);
    void normalize();
    std::vector<GMlib::DVector<GMlib::Vector<T,3>>> getData();

protected:


private:
    std::vector<GMlib::DVector<GMlib::Vector<T,3>>> _cl;
    float _lX;
    float _lY;
    float _hZ;



};

#include "pointgrid.c"

#endif // POINTGRID_H
