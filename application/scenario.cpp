#include "scenario.h"
#include "testtorus.h"
#include "pblendingcurve.h"
#include "pointgrid.h"

//// hidmanager
//#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>

// qt
#include <QQuickItem>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtWidgets/QMainWindow>



void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8, 0.002, 0.0008);
  scene()->insertLight( light, false );

  // Insert Sun
  scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 1000;
  GMlib::Point<float,3> init_cam_pos(  -50.0f, -2.0f, 25.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  0.0f, 0.0f, 1.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 1000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Front cam
  auto front_rcpair = createRCPair("Front");
  front_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, -50.0f, 0.0f ), init_cam_dir, init_cam_up );
  front_rcpair.camera->setCuttingPlanes( 1.0f, 1000.0f );
  scene()->insertCamera( front_rcpair.camera.get() );
  front_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Side cam
  auto side_rcpair = createRCPair("Side");
  side_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( -50.0f, 0.0f, 0.0f ), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ), init_cam_up );
  side_rcpair.camera->setCuttingPlanes( 1.0f, 1000.0f );
  scene()->insertCamera( side_rcpair.camera.get() );
  side_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Top cam
  auto top_rcpair = createRCPair("Top");
  top_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, 0.0f, 10.0f ), -init_cam_up, init_cam_dir );
  top_rcpair.camera->setCuttingPlanes( 1.0f, 1000.0f );
  scene()->insertCamera( top_rcpair.camera.get() );
  top_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );



  auto pg = new PointGrid<float>();
  pg->loadFile("D:/THESIS/Programming/application/data/NarvikPoints.txt");
  pg->normalize();
  auto data = pg->getData();

  std::vector<PBlendingCurve<float>*> lc_lsm;
  std::vector<PBlendingCurve<float>*> lc_vda;



  for (uint i = 0; i < data.size(); i++)
  {
      if(data[i].getDim() >= 6)
      {
          auto bspline = new PBlendingCurve<float>(data[i], 0, 2, 1);
          bspline->toggleDefaultVisualizer();
          bspline->replot(100,2);
          //std::cout << bspline->errorDiff() << std::endl;
          bspline->setColor(GMlib::GMcolor::green());
          bspline->translate(GMlib::Vector<float,3>(0,-2,0));
          scene()->insert(bspline);
          lc_lsm.push_back(bspline);
      }
  }

  for (uint i = 0; i < data.size(); i++)
  {
      if(data[i].getDim() >= 6)
      {
          auto bspline = new PBlendingCurve<float>(data[i], 2, 2, 0);
          bspline->toggleDefaultVisualizer();
          bspline->replot(100,2);
          bspline->setColor(GMlib::GMcolor::cyan());
          bspline->translate(GMlib::Vector<float,3>(0,-2,0));
          scene()->insert(bspline);
          lc_vda.push_back(bspline);
      }
  }

  std::cout << "LC Size: " << lc_lsm.size() << std::endl;

  for (uint i =0; i < lc_lsm.size(); i++)
  {
      lc_vda[i]->errorDiff();
      lc_lsm[i]->errorDiff();
  }



  std::ofstream myfile;
  myfile.open ("D:/THESIS/Data/Output/erroroutputvda.csv");
  std::ofstream myfile1;
  myfile1.open ("D:/THESIS/Data/Output/erroroutputlsm.csv");

  for (uint i =0; i < lc_lsm.size(); i++)
  {
      auto error = lc_vda[i]->getSumError();
      auto skips = lc_vda[i]->getErrorSkips();
      auto error1 = lc_lsm[i]->getSumError();
      auto skips1 = lc_lsm[i]->getErrorSkips();
      auto count = data[i].getDim();


      myfile << error[0] << "," << error[1] << "," << error[2] << "," << count << "," << skips << ",\n" ;
      myfile1 << error1[0] << "," << error1[1] << "," << error1[2] << "," << count << "," << skips1 << ",\n" ;

  }
  myfile.close();
  myfile1.close();

}

void Scenario::cleanupScenario() {

}
